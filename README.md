unknown-security
====
A security checklist based on my recommendations.

## Password
- Strong and different passphrases for Git and SSH
- Storing the passwords in a password manager, e.g. KeePass
- Changing the passwords every month

## Social
- Don't give account credentials to **anyone**! Not even people in the team

## SSH
- SSH-Keys should be, at least, 4096 Bit long
- KEEP THE KEYS SECRET
- Use different keys for SSH and Git
